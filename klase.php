<?php 

class Circle 
{
	public $name;
	public $radius;
	public $pi = 3.14;
	
	function __construct($name, $radius)
	{
		echo $name . " created." . PHP_EOL;
		$this->name = $name;
		$this->radius = $radius;
	}

	function __destruct()
	{
		echo $this->name ." destructed." . PHP_EOL;
	}

	public function returnExtent()
	{
		return 2*$this->radius*$this->pi;
	}

	public function returnSurfaceSize()
	{
		return $this->pi*($this->radius*$this->radius);
	}

	public function printData()
	{
		echo "O = " .  $this->returnExtent() . PHP_EOL;
		echo "P = " . $this->returnSurfaceSize() . PHP_EOL;
	}
}
class Triangle 
{
	public $name;
	public $base;
	public $b;
	public $c;
	public $hBase;

	// height can be empty, if empty it will be calculated
	function __construct($name, $a, $b, $c, $hBase = 0)
	{
		echo $name . " created." . PHP_EOL;
		$this->name = $name;
		$this->base = $c;
		$this->b = $b;
		$this->a = $a;
		$this->hBase = $hBase;
		if ($hBase == 0)
		{	
			$this->hBase = $this->calculateHeight();
		}
	}
	function __destruct()
	{
		echo $this->name ." destructed." . PHP_EOL;
	}

	public function returnExtent ()
	{
		return $this->base+$this->b+$this->a;
	}	

	public function returnSurfaceSize ()
	{
		return ($this->base*$this->hBase)/2;
	}

	public function calculateHeight()
	{
		//calculating height of any triangle
		$s = ($this->a+$this->b+$this->base)/2;
		return sqrt($s * ($s-$this->a) * ($s-$this->b) * ($s-$this->base)) * (2/$this->base) ;
	}

	public function printData()
	{
		echo "O = " .  $this->returnExtent() . PHP_EOL;
		echo "P = " . $this->returnSurfaceSize() . PHP_EOL;
		echo "v = " . $this->hBase . PHP_EOL;
	}
}

$trokut = new Triangle("trokut", 3,4,5);
$trokut->printData();
echo PHP_EOL;
$krug = new Circle("krug",7);
$krug->printData();